//
//  main.m
//  FoursquareExample
//
//  Created by Rafael Mendes on 29/08/14.
//  Copyright (c) 2014 Unifor. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MDEAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MDEAppDelegate class]));
    }
}
