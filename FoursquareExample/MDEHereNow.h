//
//  MDEHereNow.h
//  FoursquareExample
//
//  Created by Rafael Mendes on 29/08/14.
//  Copyright (c) 2014 Unifor. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MDEHereNow : NSObject

@property (strong, nonatomic) NSString *summary;

@end
