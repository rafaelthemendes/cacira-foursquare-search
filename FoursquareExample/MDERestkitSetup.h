//
//  MDERestkitSetup.h
//  FoursquareExample
//
//  Created by Thiago Magalhães on 03/09/14.
//  Copyright (c) 2014 Unifor. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MDERestkitSetup : NSObject

+ (id)sharedManager;
- (void)initRestKit;
@end
