//
//  MDELocalDataController.h
//  FoursquareExample
//
//  Created by Rafael Mendes on 01/09/14.
//  Copyright (c) 2014 Unifor. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RestKit/RestKit.h>

@interface MDELocalDataController : NSObject

+ (NSMutableArray *)getLocalData;

+ (void) fetchRemoteDataWithLocation:(NSString *)location success:(void (^)(RKObjectRequestOperation *operation, RKMappingResult *mappingResult))success
                 failure:(void (^)(RKObjectRequestOperation *operation, NSError *error))failure;

@end
