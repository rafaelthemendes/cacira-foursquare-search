//
//  MDEAppDelegate.m
//  FoursquareExample
//
//  Created by Rafael Mendes on 29/08/14.
//  Copyright (c) 2014 Unifor. All rights reserved.
//

#import "MDEAppDelegate.h"
#import "MDERestkitSetup.h"

@implementation MDEAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [[MDERestkitSetup sharedManager] initRestKit];
    
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // \
    Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
             // Replace this implementation with code to handle the error appropriately.
             // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        } 
    }
}

#pragma mark Restkit

- (void)configureRestKit
{
//    NSURL *baseURL = [NSURL URLWithString:@"https://api.foursquare.com"];
//    
//    AFHTTPClient *client = [[AFHTTPClient alloc] initWithBaseURL:baseURL];
//    
//    RKObjectManager *objectManager = [[RKObjectManager alloc] initWithHTTPClient:client];
//    
//    RKObjectMapping *mdeLocalMapping = [RKObjectMapping mappingForClass:[MDELocal class]];
//    
//    [mdeLocalMapping addAttributeMappingsFromArray:@[@"name", @"url"]];
//    
//    RKResponseDescriptor *responseDescriptor =
//    [RKResponseDescriptor responseDescriptorWithMapping:mdeLocalMapping
//                                                 method:RKRequestMethodGET
//                                            pathPattern:@"/v2/venues/search"
//                                                keyPath:@"response.venues"
//                                            statusCodes:[NSIndexSet indexSetWithIndex:200]];
//    
//    [objectManager addResponseDescriptor:responseDescriptor];
//    
//    // Mapeamento dos outros objetos
//    RKObjectMapping *mdeContactMapping = [RKObjectMapping mappingForClass:[MDEContact class]];
//    RKObjectMapping *mdeLocationMapping = [RKObjectMapping mappingForClass:[MDELocation class]];
//    RKObjectMapping *mdeCategoriesMapping = [RKObjectMapping mappingForClass:[MDECategories class]];
//    RKObjectMapping *mdeHereNowMapping = [RKObjectMapping mappingForClass:[MDEHereNow class]];
//    
//    //RKEntityMapping * entity = [RKEntityMapping mappingForEntityForName:@"MDELocal" inManagedObjectStore:[RKManagedObjectStore defaultStore]];
//    
//    [mdeContactMapping addAttributeMappingsFromArray:@[@"phone", @"formattedPhone", @"twitter"]];
//    
//    [mdeLocationMapping addAttributeMappingsFromArray:@[@"address", @"city", @"country", @"crossStreet",
//                                                        @"postalCode", @"state", @"distance", @"lat", @"lng"]];
//    
//    [mdeCategoriesMapping addAttributeMappingsFromArray:@[@"pluralName"]];
//    
//    [mdeHereNowMapping addAttributeMappingsFromArray:@[@"summary"]];
//    
//    
//    // Definindo relacionamentos
//    [mdeLocalMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"contact"
//                                                                                    toKeyPath:@"contact"
//                                                                                  withMapping:mdeContactMapping]];
//    
//    [mdeLocalMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"location"
//                                                                                    toKeyPath:@"location"
//                                                                                  withMapping:mdeLocationMapping]];
//    
//    [mdeLocalMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"categories"
//                                                                                    toKeyPath:@"categories"
//                                                                                  withMapping:mdeCategoriesMapping]];
//    
//    [mdeLocalMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"hereNow"
//                                                                                    toKeyPath:@"hereNow"
//                                                                                  withMapping:mdeHereNowMapping]];
    
}

@end
