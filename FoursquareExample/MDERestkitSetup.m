//
//  MDERestkitSetup.m
//  FoursquareExample
//
//  Created by Thiago Magalhães on 03/09/14.
//  Copyright (c) 2014 Unifor. All rights reserved.
//

#import "MDERestkitSetup.h"


@implementation MDERestkitSetup
{
    @private RKObjectManager * objectManager;
}

#define kBaseURL @"https://api.foursquare.com"
#define kVenuesURL @"/v2/venues/search"

+ (id)sharedManager {
    static MDERestkitSetup * sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (void)initRestKit
{
    NSURL * baseURL = [NSURL URLWithString:kBaseURL];
    
    objectManager = [RKObjectManager managerWithBaseURL:baseURL];
    
    objectManager.requestSerializationMIMEType = RKMIMETypeJSON;
    
    [[objectManager HTTPClient] setDefaultHeader:@"content-type" value:RKMIMETypeJSON];
    
    [AFNetworkActivityIndicatorManager sharedManager].enabled = YES;
    
    NSManagedObjectModel *managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];
    
    RKManagedObjectStore *managedObjectStore = [[RKManagedObjectStore alloc] initWithManagedObjectModel:managedObjectModel];
    
    objectManager.managedObjectStore = managedObjectStore;
    
    [self initCoreData];
    
    [self initResponseDescriptors];
}

- (void)initCoreData
{
    NSManagedObjectModel *managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];
    
    RKManagedObjectStore *managedObjectStore = [[RKManagedObjectStore alloc] initWithManagedObjectModel:managedObjectModel];
    
    objectManager.managedObjectStore = managedObjectStore;
    
    [managedObjectStore createPersistentStoreCoordinator];
    
    NSString *storePath = [RKApplicationDataDirectory() stringByAppendingPathComponent:@"FourSquareExample.sqlite"];
    
    NSError *error;
    
    NSPersistentStore * persistentStore = [managedObjectStore addSQLitePersistentStoreAtPath:storePath fromSeedDatabaseAtPath:nil withConfiguration:nil options:nil error:&error];
    
    NSAssert(persistentStore, @"Failed to add persistent store with error: %@", error);
    
    [managedObjectStore createManagedObjectContexts];
    
    managedObjectStore.managedObjectCache = [[RKInMemoryManagedObjectCache alloc] initWithManagedObjectContext:objectManager.managedObjectStore.persistentStoreManagedObjectContext];
    
    [RKManagedObjectStore setDefaultStore:managedObjectStore];
}

- (void)initResponseDescriptors
{
    NSArray * responseDescriptors = [self responseDescriptors];
    
    [objectManager addResponseDescriptorsFromArray: responseDescriptors];
}

- (NSArray *) responseDescriptors
{
    
    RKResponseDescriptor * getVenuesDescriptor;
    
    getVenuesDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:[self venueMapping]
                                                              method:RKRequestMethodGET
                                                         pathPattern:kVenuesURL
                                                             keyPath:@"response.venues"
                                                         statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    
    return @[getVenuesDescriptor];
}

- (RKEntityMapping *) venueMapping
{
    RKEntityMapping * mapping = [RKEntityMapping mappingForEntityForName:@"MDELocal" inManagedObjectStore:objectManager.managedObjectStore];
    
    [mapping addAttributeMappingsFromArray:@[@"name",
                                             @"url"]];
    
    RKRelationshipMapping * contactMapping = [RKRelationshipMapping relationshipMappingFromKeyPath:@"contact" toKeyPath:@"contact" withMapping:[self contactMapping]];
    
    RKRelationshipMapping * locationMapping = [RKRelationshipMapping relationshipMappingFromKeyPath:@"location" toKeyPath:@"location" withMapping:[self locationMapping]];
    
    RKRelationshipMapping * categoryMapping = [RKRelationshipMapping relationshipMappingFromKeyPath:@"category" toKeyPath:@"category" withMapping:[self categoryMapping]];
    
    RKRelationshipMapping * hereNowMapping = [RKRelationshipMapping relationshipMappingFromKeyPath:@"hereNow" toKeyPath:@"hereNow" withMapping:[self hereNowMapping]];
    
    [mapping addPropertyMappingsFromArray:@[contactMapping,
                                            locationMapping,
                                            categoryMapping,
                                            hereNowMapping]];
    
    
    return mapping;
}

- (RKEntityMapping *) contactMapping
{
    RKEntityMapping * mapping = [RKEntityMapping mappingForEntityForName:@"MDEContact" inManagedObjectStore:objectManager.managedObjectStore];
    
     [mapping addAttributeMappingsFromArray:@[@"phone",
                                              @"formattedPhone",
                                              @"twitter"]];
    
    return mapping;
}

- (RKEntityMapping *) locationMapping
{
    RKEntityMapping * mapping = [RKEntityMapping mappingForEntityForName:@"MDELocation" inManagedObjectStore:objectManager.managedObjectStore];
    
    [mapping addAttributeMappingsFromArray:@[@"address",
                                             @"city",
                                             @"country",
                                             @"crossStreet",
                                             @"postalCode",
                                             @"state",
                                             @"distance",
                                             @"lat",
                                             @"lng"]];
    return mapping;
}

- (RKEntityMapping *) categoryMapping
{
    RKEntityMapping * mapping = [RKEntityMapping mappingForEntityForName:@"MDECategory" inManagedObjectStore:objectManager.managedObjectStore];
    
    [mapping addAttributeMappingsFromArray:@[@"pluralName"]];
    
    return mapping;
}

- (RKEntityMapping *) hereNowMapping
{
    RKEntityMapping * mapping  = [RKEntityMapping mappingForEntityForName:@"MDEHereNow" inManagedObjectStore:objectManager.managedObjectStore];
    
    [mapping addAttributeMappingsFromArray:@[@"summary"]];
    
    return mapping;
}



@end
