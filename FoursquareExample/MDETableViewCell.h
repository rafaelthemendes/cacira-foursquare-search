//
//  MDETableViewCell.h
//  FoursquareExample
//
//  Created by Rafael Mendes on 02/09/14.
//  Copyright (c) 2014 Unifor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MDETableViewCell : UITableViewCell

@end
