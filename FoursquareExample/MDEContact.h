//
//  MDEContact.h
//  FoursquareExample
//
//  Created by Rafael Mendes on 29/08/14.
//  Copyright (c) 2014 Unifor. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MDEContact : NSObject

@property (strong, nonatomic) NSNumber *phone;
@property (strong, nonatomic) NSString *formattedPhone;
@property (strong, nonatomic) NSString *twitter;

@end
