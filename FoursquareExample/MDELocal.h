//
//  MDELocal.h
//  FoursquareExample
//
//  Created by Rafael Mendes on 29/08/14.
//  Copyright (c) 2014 Unifor. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MDELocation.h"
#import "MDEContact.h"
#import "MDEHereNow.h"

@class MDELocation;

@interface MDELocal : NSObject

//mesmo nome que vem no JSON
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *url;

@property (strong, nonatomic) MDELocation *location;
@property (strong, nonatomic) MDEContact *contact;
@property (strong, nonatomic) MDEHereNow *hereNow;
@property (strong, nonatomic) NSArray *categories;

@end

