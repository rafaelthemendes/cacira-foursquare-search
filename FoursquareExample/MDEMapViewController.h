//
//  MDEMapViewController.h
//  FoursquareExample
//
//  Created by Rafael Mendes on 01/09/14.
//  Copyright (c) 2014 Unifor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

#import <MapKit/MapKit.h>

#import <RestKit/RestKit.h>
#import <CoreData/CoreData.h>

@interface MDEMapViewController : UIViewController <MKMapViewDelegate>

@property (strong, nonatomic) IBOutlet MKMapView *mapView;

@end
