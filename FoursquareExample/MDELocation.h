//
//  MDELocation.h
//  FoursquareExample
//
//  Created by Rafael Mendes on 29/08/14.
//  Copyright (c) 2014 Unifor. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MDELocation : NSObject

@property (strong, nonatomic) NSString *address;
@property (strong, nonatomic) NSString *city;
@property (strong, nonatomic) NSString *country;
@property (strong, nonatomic) NSString *crossStreet;
@property (strong, nonatomic) NSString *postalCode;
@property (strong, nonatomic) NSString *state;
@property (strong, nonatomic) NSNumber *distance;
@property (strong, nonatomic) NSNumber *lat;
@property (strong, nonatomic) NSNumber *lng;

@end
