//
//  MDECategories.h
//  FoursquareExample
//
//  Created by Rafael Mendes on 31/08/14.
//  Copyright (c) 2014 Unifor. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MDECategories : NSObject

@property (strong, nonatomic) NSString *pluralName;

@end
