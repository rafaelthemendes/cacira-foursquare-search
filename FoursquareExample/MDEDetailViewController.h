//
//  MDEDetailViewController.h
//  FoursquareExample
//
//  Created by Rafael Mendes on 29/08/14.
//  Copyright (c) 2014 Unifor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <RestKit/RestKit.h>
#import "MDELocal.h"

@interface MDEDetailViewController : UIViewController

@property (strong, nonatomic) id local;
@property (weak, nonatomic) IBOutlet UIScrollView * scrollView;

@end
