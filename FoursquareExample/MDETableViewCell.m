//
//  MDETableViewCell.m
//  FoursquareExample
//
//  Created by Rafael Mendes on 02/09/14.
//  Copyright (c) 2014 Unifor. All rights reserved.
//

#import "MDETableViewCell.h"

@implementation MDETableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
