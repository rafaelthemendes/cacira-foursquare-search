//
//  MDELocalDataController.m
//  FoursquareExample
//
//  Created by Rafael Mendes on 01/09/14.
//  Copyright (c) 2014 Unifor. All rights reserved.
//

#import "MDELocalDataController.h"

// Foursquare Credentials
#define keyCLIENTID @"Z1ULU2FVYRXE2ASFYX5BJKZTZVDFUT0MPMMK4IIUGMGNVMCO"
#define keyCLIENTSECRET @"PSZOXANR1C2D2KRWFVITVUWTMZAHWJAY2PZG4N1ZOUCOUH40"

@implementation MDELocalDataController

+ (NSMutableArray *)getLocalData
{
    NSManagedObjectContext *moc = [RKManagedObjectStore defaultStore].persistentStoreManagedObjectContext;
    
    NSEntityDescription *entityDescription = [NSEntityDescription
                                              entityForName:@"MDELocal" inManagedObjectContext:moc];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    
    [request setEntity:entityDescription];
    
    NSError *error;
    
    NSArray *array = [moc executeFetchRequest:request error:&error];
    
    return [array mutableCopy];
}

+ (void) fetchRemoteDataWithLocation:(NSString *)location success:(void (^)(RKObjectRequestOperation *operation, RKMappingResult *mappingResult))success
                        failure:(void (^)(RKObjectRequestOperation *operation, NSError *error))failure
{
    
    NSString *clientID = keyCLIENTID;
    NSString *clientSecret = keyCLIENTSECRET;
    
    
    //Caminho da busca
    NSString *path = @"/v2/venues/search";
    
    //Parametros da requisição
    NSDictionary *queryParams = @{@"ll" : location,
                                  @"client_id" : clientID,
                                  @"client_secret" : clientSecret,
                                  @"v" : @"20140829"};
    
    // Fetch
    [[RKObjectManager sharedManager] getObjectsAtPath: path
                                           parameters:queryParams
                                              success:success
                                              failure:failure];
}


@end
