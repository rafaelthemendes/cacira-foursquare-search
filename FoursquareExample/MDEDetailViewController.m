//
//  MDEDetailViewController.m
//  FoursquareExample
//
//  Created by Rafael Mendes on 29/08/14.
//  Copyright (c) 2014 Unifor. All rights reserved.
//

#import "MDEDetailViewController.h"

#define URL_IMAGE @"http://files.empreendemia.com.br.s3-sa-east-1.amazonaws.com/companies/19116/1911663967ca4bd902fee6bfda1be8c1e76e586e05b33c2b/200_thumbnail.png"

@interface MDEDetailViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UILabel *webSiteLabel;
@property (weak, nonatomic) IBOutlet UILabel *contactLabel;
@property (weak, nonatomic) IBOutlet UILabel *twitterLabel;

- (void)configureView;
@end

@implementation MDEDetailViewController

- (void)viewDidLoad
{
    [self.scrollView setScrollEnabled:YES];
    [self.scrollView setContentSize:CGSizeMake(240, 300)];
    
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    [self configureView];
}

- (void)configureView
{
    // Update the user interface for the detail item.

    NSLog(@"Configuring View!!!");
    if (self.local) {
        
        //Carregando assincronamente a imagem
        [self.imageView setImageWithURL:[NSURL URLWithString:URL_IMAGE]];
        
        MDELocal *local = (MDELocal *)self.local;
        
        self.nameLabel.text = local.name;
                

        self.addressLabel.text = local.location.address;
        
        if(local.location.distance)
            self.distanceLabel.text = [NSString stringWithFormat:@"Distância: %@ ft", local.location.distance];
        else
            self.distanceLabel.text = @"";
        
        if(local.hereNow.summary)
            self.statusLabel.text = [NSString stringWithFormat:@"Status: %@", local.hereNow.summary];
        else
            self.statusLabel.text = @"";
        
        
        if(local.url)
            self.webSiteLabel.text = [NSString stringWithFormat:@"Web site: %@",local.url];
        else
            self.webSiteLabel.text = @"";
        
        if(local.contact.formattedPhone)
        self.contactLabel.text = [NSString stringWithFormat:@"Contato: %@",local.contact.formattedPhone];
        else
            self.contactLabel.text = @"";
        
        if(local.contact.twitter)
            self.twitterLabel.text = [NSString stringWithFormat:@"Twitter: %@",local.contact.twitter];
        else
            self.twitterLabel.text = @"";
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
