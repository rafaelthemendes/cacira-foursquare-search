//
//  MDEMasterViewController.m
//  FoursquareExample
//
//  Created by Rafael Mendes on 29/08/14.
//  Copyright (c) 2014 Unifor. All rights reserved.
//

#import "MDETableViewController.h"
#import "MDEDetailViewController.h"


#import "MDELocalDataController.h"

#import "MDELocal.h"
#import "MDELocation.h"
#import "MDEContact.h"
#import "MDECategories.h"

static NSString *SectionsTableIdentifier = @"CellIdentifier";

@interface MDETableViewController (){
    NSString * _location;
}

@property (strong, nonatomic) CLLocationManager *locationManager;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicatorView;

//Array que armazena os locais retornados pela requisição
@property (copy, nonatomic) NSArray *keys;
@property (copy, nonatomic) NSDictionary *venues;

@end

@implementation MDETableViewController{
    NSMutableArray *filteredNames;
    UISearchDisplayController *searchController;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    //todo: registrar uma nib
    //Registrando celulas para facilitar o reuso ao dar scroll na tableview. Desta forma não consigo mudar o estilo da célula.
//    UITableView *tableView = (id)[self.view viewWithTag:1];
//    [tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:SectionsTableIdentifier];

    
    //Configurando search bar
    filteredNames = [NSMutableArray array];
    UISearchBar *searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    self.tableView.tableHeaderView = searchBar;
    
    //Configurando search controller
    searchController = [[UISearchDisplayController alloc] initWithSearchBar:searchBar contentsController:self];
    searchController.delegate = self;
    searchController.searchResultsDataSource = self;
    
    //Iniciando serviço de localização
    [self configureLocation];
    
    //Setando indicador de progresso
    [self.activityIndicatorView startAnimating];
   
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Location

- (void)configureLocation
{
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [self.locationManager startUpdatingLocation];
}


#pragma mark - CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil) {
        
        NSString * location = [NSString stringWithFormat:@"%f, %f", currentLocation.coordinate.latitude, currentLocation.coordinate.longitude];
        
        _location = location;
        
        [self loadVenues];
    }
    [self.locationManager stopUpdatingLocation];
    
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"Erro"
                                                         message:@"Não foi possível carregar localização"
                                                        delegate:nil
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil];
    [errorAlert show];
    
    //[self hideIndicator];
}



#pragma mark - Table View
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //Se for a tableview, retorna o numero de categorias, caso seja a searchbar, retorna 1
    if (tableView.tag == 1) {
        return [self.keys count];
    } else {
        return 1;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView.tag == 1){
        NSString *key = self.keys[section];
        NSArray *venuesSection = self.venues[key];
        return [venuesSection count];
    } else {
        return [filteredNames count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:SectionsTableIdentifier];
    
    if(cell == nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:SectionsTableIdentifier];
    
    MDELocal *venue;
    if (tableView.tag == 1) {
        NSString *key = self.keys[indexPath.section];
        NSArray *nameSection = self.venues[key];
        venue = nameSection[indexPath.row];
    } else {
        venue = filteredNames[indexPath.row];
    }
    
    cell.imageView.image = [UIImage imageNamed:@"foursquare-icon.png"];
    cell.textLabel.text = venue.name;
    cell.detailTextLabel.text = venue.location.address;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (tableView.tag == 1) {
        return self.keys[section];
    } else {
        return nil;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self performSegueWithIdentifier:@"showDetail" sender:tableView];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSLog(@"PrepareForSegue!!!");
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        UITableView *tableView = sender;
        NSIndexPath *indexPath = [tableView indexPathForSelectedRow];
        MDELocal *venue;
        
        if(tableView.tag == 1){
            NSString *key = self.keys[indexPath.section];
            NSArray *venueSection = self.venues[key];
            venue = venueSection[indexPath.row];
        }else{
            venue = filteredNames[indexPath.row];
        }
        
        [[segue destinationViewController] setLocal:venue];
    }
}

#pragma mark Search Display Controller
- (void)searchDisplayController:(UISearchDisplayController *)controller
  didLoadSearchResultsTableView:(UITableView *)tableView
{
    tableView.delegate = self;
}


- (BOOL)searchDisplayController:(UISearchDisplayController *)controller
shouldReloadTableForSearchString:(NSString *)searchString
{
    [filteredNames removeAllObjects];
    if (searchString.length > 0) {
        NSPredicate *predicate =
        [NSPredicate
         predicateWithBlock:^BOOL(MDELocal *local, NSDictionary *b) {
             NSString *name = local.name;
             NSRange range = [name rangeOfString:searchString
                                         options:NSCaseInsensitiveSearch];
             return range.location != NSNotFound;
         }];
        for (NSString *key in self.keys) {
            NSArray *venues = [self.venues valueForKey:key];
            NSArray *matches = [venues filteredArrayUsingPredicate: predicate];
            [filteredNames addObjectsFromArray:matches];
        }
    }
    return YES;
}


#pragma mark - Fetched results controller
- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Event" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"timeStamp" ascending:NO];
    NSArray *sortDescriptors = @[sortDescriptor];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:@"Master"];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
	NSError *error = nil;
	if (![self.fetchedResultsController performFetch:&error]) {
	     // Replace this implementation with code to handle the error appropriately.
	     // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
	    NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
	    abort();
	}
    
    return _fetchedResultsController;
}    

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    NSManagedObject *object = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.textLabel.text = [[object valueForKey:@"timeStamp"] description];
}


-(void)loadVenues
{
//    NSArray *data = [MDELocalDataController getLocalData];
//    
//    if(data){
//        NSLog(@"Local Data Loaded!!!");
//        [self setVenueDictionaryWithArray:data];
//        [self.tableView reloadData];
//        [self hideIndicator];
//    }
    
    [self loadRemoteVenues];
}

- (void)loadRemoteVenues
{
    [MDELocalDataController fetchRemoteDataWithLocation:_location
                                                success:
     //Bloco executado quando a resposta chega corretamente
     ^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
         NSLog(@"Result OK!!!");
         [self setVenueDictionaryWithArray:mappingResult.array];
         [self.tableView reloadData];
         [self hideIndicator];
     }
                                                failure:
     //Bloco executado quando há falha na requisição
     ^(RKObjectRequestOperation *operation, NSError *error) {
         NSLog(@"Houve um erro na requisição: %@", error);
         [self hideIndicator];
         
     }];
    
}

- (void)setVenueDictionaryWithArray:(NSArray *)array
{
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    for(MDELocal *venue in array){
        
        for(MDECategories *category in venue.categories){
            
            //cria a key caso não exista
            if ([dictionary objectForKey:category.pluralName] == nil) {
                [dictionary setValue:[NSMutableArray array] forKey:category.pluralName];
            }
            
            //agora insere o local no array desta key
            NSMutableArray *mutableArray = [dictionary valueForKey:category.pluralName];
            [mutableArray addObject:venue];
        }
    }
    self.venues = [dictionary copy];
    self.keys = [[dictionary allKeys] sortedArrayUsingSelector:@selector(compare:)];
}

- (void)hideIndicator
{
    self.activityIndicatorView.hidesWhenStopped = YES;
    [self.activityIndicatorView stopAnimating];
}

@end
