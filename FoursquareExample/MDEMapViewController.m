//
//  MDEMapViewController.m
//  FoursquareExample
//
//  Created by Rafael Mendes on 01/09/14.
//  Copyright (c) 2014 Unifor. All rights reserved.
//

#import "MDEMapViewController.h"
#import "MDELocalDataController.h"

#import "MDELocal.h"
#import "MDEDetailViewController.h"

#define regionSpan 400

@interface MDEMapViewController (){
    MKUserLocation * _currentUserLocation;
    NSString * _locationString;
    
    NSMutableDictionary *_venuePointAnnotations;
}

@property (strong, nonatomic) NSArray *venues;

@end

@implementation MDEMapViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //Mostrando a posição atual do usuário
    _mapView.showsUserLocation = YES;
    _mapView.mapType = MKMapTypeSatellite;
    _mapView.delegate = self;

    //Centralizando a posição do usuário
    _currentUserLocation = _mapView.userLocation;
    
    _locationString = [NSString stringWithFormat:@"%f,%f", _currentUserLocation.location.coordinate.latitude, _currentUserLocation.location.coordinate.longitude];
    
    NSLog(@"User Location: %f, %f", _currentUserLocation.location.coordinate.latitude, _currentUserLocation.location.coordinate.longitude);
    
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(_currentUserLocation.location.coordinate, regionSpan, regionSpan);
    [_mapView setRegion:region animated:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - MKMapViewDelegate
- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    NSLog(@"New Coordinates: <%f, %f>", userLocation.location.coordinate.latitude, userLocation.location.coordinate.longitude);
    
    _locationString = [NSString stringWithFormat:@"%f,%f", userLocation.location.coordinate.latitude, userLocation.location.coordinate.longitude];
    
    _mapView.centerCoordinate = userLocation.location.coordinate;

    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLocation.location.coordinate, regionSpan, regionSpan);
    [_mapView setRegion:region animated:NO];
    
    [self loadRemoteVenues];

}

- (void) configPointAnnotations
{
    _venuePointAnnotations = [[NSMutableDictionary alloc]init];
    
    [_mapView removeAnnotations:_mapView.annotations];
    for(int i = 0 ; i < [self.venues count]; i++)
    {
        MDELocal * venue = self.venues[i];
        
        NSLog(@"New annotation!!");
        MKPointAnnotation *annotation = [[MKPointAnnotation alloc]init];
        
        annotation.title = venue.name;
        annotation.subtitle = venue.hereNow.summary;
        annotation.coordinate = CLLocationCoordinate2DMake([venue.location.lat doubleValue], [venue.location.lng doubleValue]);
        
        [_venuePointAnnotations setObject:[NSNumber numberWithInt:i]  forKey:[NSString stringWithFormat:@"%f%f",annotation.coordinate.latitude, annotation.coordinate.longitude]];
        [_mapView addAnnotation:annotation];
    }
}

#pragma mark - MKAnnotation
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
        
    NSLog(@"ViewForAnnotation!!!");
    
    //Se usar outros tipos de annotation, inserir na decisão
    if([annotation isKindOfClass: [MKUserLocation class]])
        return nil;
    
    static NSString *identifier = @"MyLocation";
    
    MKPinAnnotationView *annotationView = (MKPinAnnotationView *) [mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
    
    if (annotationView == nil) {
        annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
    } else {
        annotationView.annotation = annotation;
    }
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    annotationView.rightCalloutAccessoryView = button;
    
    UIImageView *image = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"map.png"]];
    annotationView.leftCalloutAccessoryView = image;
    
    annotationView.enabled = YES;
    annotationView.canShowCallout = YES;
    
    NSNumber *index = [_venuePointAnnotations objectForKey:[NSString stringWithFormat:@"%f%f",annotation.coordinate.latitude, annotation.coordinate.longitude]];
    
    annotationView.tag = [index intValue];
    
    return annotationView;
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control {
    
    [self performSegueWithIdentifier:@"showDetail" sender:view];
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSLog(@"Prepare for segue!!!");
    
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        MKPinAnnotationView *view = sender;
        
        MDELocal *venue = self.venues[view.tag];
        
        [[segue destinationViewController]  setLocal:venue];
    }
}


#pragma mark - Restkit Request
- (void)loadRemoteVenues
{
    [MDELocalDataController fetchRemoteDataWithLocation:_locationString
                                                success:
     
     //Bloco executado quando a resposta chega corretamente
     ^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
         NSLog(@"Result OK!!!");
         
         self.venues = mappingResult.array;
         
         [self configPointAnnotations];
     }
                                                failure:
     ^(RKObjectRequestOperation *operation, NSError *error) {
         NSLog(@"Houve um erro na requisição: %@", error);
     }];
}

@end
